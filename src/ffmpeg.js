const spawn = require('child_process').spawn
const events = require('events')
const fs = require('fs')

class FFMPEG extends events.EventEmitter {
    constructor(opts, channel) {
        super()
        this.offset = 0
        this.args = []
        this.opts = opts
        this.channel = channel
        this.ffmpegPath = opts.ffmpegPath
        let lines = opts.args.split('\n')
        for (let i = 0, l = lines.length; i < l; i++) {
            let x = lines[i].indexOf(' ')
            if (x === -1)
                this.args.push(lines[i])
            else {
                this.args.push(lines[i].substring(0, x))
                this.args.push(lines[i].substring(x + 1, lines[i].length))
            }
        }
    }
    // This is used to generate ass subtitles from text subs to be used with the ass filter in ffmpeg.
    createSubsFromStream(file, startTime, duration, streamIndex, output) {
        if (process.env.DEBUG) console.log('Generating .ass subtitles')
        let exe = spawn(this.ffmpegPath, [
            '-threads', this.opts.threads,
            '-ss', startTime,
            '-i', file,
            '-t', duration,
            '-map', `0:${streamIndex}`,
            '-f', 'ass',
            output
        ])
        return new Promise((resolve, reject) => {
            if (this.opts.logFfmpeg) {
                exe.stderr.on('data', (chunk) => {
                    process.stderr.write(chunk)
                })
            }
            exe.on('close', (code) => {
                if (code === 0) {
                    if (process.env.DEBUG) console.log('Successfully generated .ass subtitles')
                    resolve()
                } else {
                    console.log('Failed generating .ass subtitles.')
                    reject()
                }
            })
        })
    }
    async spawn(lineupItem) {
        let videoIndex = lineupItem.opts.videoIndex
        let audioIndex = lineupItem.opts.audioIndex
        let subtitleIndex = lineupItem.opts.subtitleIndex
        let uniqSubFileName = Date.now().valueOf().toString()

        for (let i = 0, l = lineupItem.streams.length; i < l; i++) {
            if (videoIndex === '-1' && lineupItem.streams[i].streamType === 1)
                if (lineupItem.streams[i].default)
                    videoIndex = i
            if (audioIndex === '-1' && lineupItem.streams[i].streamType === 2)
                if (lineupItem.streams[i].default || lineupItem.streams[i].selected)
                    audioIndex = i
            if (subtitleIndex === '-1' && lineupItem.streams[i].streamType === 3)
                if (lineupItem.streams[i].default || lineupItem.streams[i].forced)
                    subtitleIndex = i
        }

        // if for some reason we didn't find a default track, let ffmpeg decide..
        if (videoIndex === '-1')
            videoIndex = 'v'
        if (audioIndex === '-1')
            audioIndex === 'a'

        let tmpargs = JSON.parse(JSON.stringify(this.args))
        let startTime = tmpargs.indexOf('STARTTIME')
        let dur = tmpargs.indexOf('DURATION')
        let input = tmpargs.indexOf('INPUTFILE')
        let vidStream = tmpargs.indexOf('VIDEOSTREAM')
        let output = tmpargs.indexOf('OUTPUTFILE')
        let tsoffset = tmpargs.indexOf('TSOFFSET')
        let audStream = tmpargs.indexOf('AUDIOSTREAM')
        let chanName = tmpargs.indexOf('CHANNELNAME')

        tmpargs[startTime] = lineupItem.start / 1000
        tmpargs[dur] = lineupItem.duration / 1000
        tmpargs[input] = lineupItem.file
        tmpargs[audStream] = `0:${audioIndex}`
        tmpargs[chanName] = `service_name="${this.channel.name}"`
        tmpargs[tsoffset] = this.offset
        tmpargs[output] = 'pipe:1'

        let audChannels = lineupItem.streams[audioIndex].channels
        let audCodec = lineupItem.streams[audioIndex].codec
        let audSettingsIndex = tmpargs.indexOf('-c:a') + 1

        // Transcode audio if necessary
        if (audChannels > 6) {
            console.log("More than 6 audio channels! Transcoding audio to 5.1")
            if (audCodec == "aac") {
                tmpargs.splice(audSettingsIndex, 1, "aac", "-ac", "6")
            } else if (audCodec == "ac3") {
                tmpargs.splice(audSettingsIndex, 1, "ac3", "-ac", "6")
            } else {
                tmpargs.splice(audSettingsIndex, 1, "ac3", "-ac", "6", "-ar", "48000", "-b:a", "336k")   
            }          
        } else if (audCodec != "ac3" && audCodec != "aac") {
            if (audChannels >= 6) {
                tmpargs.splice(audSettingsIndex, 1, "ac3", "-ac", "6", "-ar", "48000", "-b:a", "336k")
            } else {
                // Last transcoding scenario, just go to 2 channel audio, boost center channel by 4db if its a downmix. Low dialog loud explisions suck.
                tmpargs.splice(audSettingsIndex, 1, "ac3", "-ac", "2", "-ar", "48000", "-b:a", "192k", "-vol", "425")
            } 
        }

        this.offset += lineupItem.duration / 1000
        this.ffmpeg = spawn(this.ffmpegPath, tmpargs)
        this.ffmpeg.stdout.on('data', (chunk) => {
            this.emit('data', chunk)
        })
        if (this.opts.logFfmpeg) {
            this.ffmpeg.stderr.on('data', (chunk) => {
                process.stderr.write(chunk)
            })
        }
        this.ffmpeg.on('close', (code) => {
            if (fs.existsSync(`${process.env.DATABASE}/${uniqSubFileName}.ass`))
                fs.unlinkSync(`${process.env.DATABASE}/${uniqSubFileName}.ass`)
            if (code === null || code === 255)
                this.emit('close', code)
            else if (code === 0)
                this.emit('end')
            else
                this.emit('error', { code: code, cmd: `${tmpargs.join(' ')}` })
        })
    }
    kill() {
        if (typeof this.ffmpeg != "undefined") {
            this.ffmpeg.kill('SIGQUIT')
        }
    }
}

module.exports = FFMPEG